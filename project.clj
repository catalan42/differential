(defproject hello  "0.1.0-SNAPSHOT"
  :description    "FIXME: write description"
  :url            "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies
  [
   [hiccup "1.0.5"]
   [http-kit "2.3.0"]
   [io.pedestal/pedestal.jetty "0.5.5"]
   [io.pedestal/pedestal.route "0.5.5"]
   [io.pedestal/pedestal.service "0.5.5"]
   [org.clojure/clojure "1.10.0"]
   [org.clojure/data.json "0.2.6"]
   [org.clojure/test.check "0.9.0"]
   [org.slf4j/slf4j-simple "1.7.25"]
   [prismatic/schema "1.1.9"]
   [tupelo "0.9.120"]
   ]
  :profiles {:dev {:dependencies []
                   :plugins [
                             [com.jakemccrary/lein-test-refresh   "0.22.0"]
                             [lein-ancient                        "0.6.15"]
                             [lein-codox                          "0.10.3"] ] }
             :uberjar {:aot :all}}

  :global-vars {*warn-on-reflection* false}
  :main ^:skip-aot hello.core

  :source-paths       ["src"]
  :test-paths         ["test"]
  :java-source-paths  ["src-java"]
  :target-path        "target/%s"
  :jvm-opts           ["-Xms500m" "-Xmx2g" ]  ; "--add-modules" "java.xml.bind"  ; if needed re Java 9+
)
