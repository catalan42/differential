(ns hello.core
  (:use tupelo.core)
  (:require
    [clojure.data.json :as json]
    [clojure.string :as str]
    [hiccup.core :as hiccup]
    [io.pedestal.http :as http]
    [io.pedestal.http.content-negotiation :as conneg]
    [io.pedestal.http.route :as route]
    [schema.core :as s]
    [tupelo.parse :as tpar]
    [tupelo.pedestal :as tp]
    [tupelo.schema :as tsk]))

; Prismatic Schema type definitions
(s/set-fn-validation! true) ; enforce fn schemas

(defn ok [body]
  {:status 200 :body body})

(defn not-found []
  {:status 404 :body "Not found\n"})

(tp/definterceptor echo-intc
  {:enter (s/fn [context :- tp/Context] ; interceptor :enter/:leave receives a context (not request)
            (let [response         (ok (pretty-str (glue (sorted-map) (tpar/edn-parsible context)))) ]
              (into context {:response response})))})

(def supported-types [tp/text-html tp/application-edn tp/application-json tp/text-plain] )

(def content-negotiation-intc
  (validate tp/interceptor?
    (conneg/negotiate-content supported-types)))

(defn negotiated-type
  "Returns the Content-Type determined by the content negotiation interceptor"
  [ctx]
  (get-in ctx [:request :accept :field] "text/plain"))

(defn transform-content
  [body content-type]
  (condp = content-type
    tp/text-html         body
    tp/text-plain        body
    tp/application-edn   (pr-str body)
    tp/application-json  (json/write-str body)))

(defn coerce-to
  [response content-type]
  (-> response
    (update :body transform-content content-type)
    (assoc-in [:headers "Content-Type"] content-type)))

(defn differential
  [term]
  (with-map-vals term [coeff exp]
    {:coeff (* coeff exp) :exp (dec exp)}))

(defn term->str
  [term]
  (with-map-vals term [coeff exp]
    (cond
      (zero? coeff) nil
      (= 0 exp) (format "%d" coeff)
      (= 1 exp) (format "%dx" coeff)
      :else (format "%dx^%d" coeff exp))))

(defn terms->str [terms]
  (let [result (str/join "+" (remove nil? (mapv term->str terms)))]
    (if (empty? result)
      "0"
      result)))

(defn coeffs->terms [coeffs]
  (let [order      (count coeffs)
        exponents  (reverse (range order))
        poly-terms (forv [[coeff exp] (zip coeffs exponents)]
                     {:coeff coeff :exp exp})]
    poly-terms))

(defn differential-str [coeffs]
  (let [poly-terms-diff (mapv differential (coeffs->terms coeffs))
        poly-str-diff   (terms->str poly-terms-diff)]
    poly-str-diff))

(tp/definterceptor differentiate-intc
  {:leave (fn [context]
            (let [[-trash- path-params-str] (first (fetch-in context [:request :path-params]))
                  path-params-vals (str/split path-params-str #"/")
                  coeffs           (mapv tpar/parse-int path-params-vals)
                  poly-terms-orig  (coeffs->terms coeffs)
                  poly-str-orig    (terms->str poly-terms-orig)
                  poly-str-diff    (differential-str coeffs)
                  html-txt         (hiccup/html
                                     [:div {:font-family "monospace"}
                                      [:div (str "Original   Polynomial:  " poly-str-orig)]
                                      [:div (str "Derivative Polynomial:  " poly-str-diff)]])
                  response         (ok html-txt)]
              (glue context {:response (coerce-to response tp/text-html)})))})

(tp/definterceptor differentiate-api-intc
  {:leave (fn [context]
            (let [[-trash- path-params-str] (first (fetch-in context [:request :path-params]))
                  path-params-vals (str/split path-params-str #"/")
                  coeffs           (mapv tpar/parse-int path-params-vals)
                  poly-terms-orig  (coeffs->terms coeffs)
                  poly-str-diff    (differential-str coeffs)
                  response         (ok {:differential poly-str-diff})]
              (glue context {:response (coerce-to response tp/application-json)})))})

(defn index-handler
  [request]
  (validate tp/request? request)
  (let [html-txt (hiccup/html
                   [:div
                    [:div "Try the following:"]
                    [:ul
                     [:li [:a {:href "http://localhost:8890/differentiate/3/2/1"} "Derivative of 3x ^2 + 2x + 1"]]
                     ]])]
    (let [response (ok html-txt)]
      (coerce-to response tp/text-html))))

(def server-routes
  (route/expand-routes
    #{
      (tp/table-route {:verb :get :path "/"  :route-name :index  :interceptors index-handler})
      (tp/table-route {:verb :get :path "/echo/*"  :route-name :echo  :interceptors echo-intc})
      (tp/table-route {:verb         :get :path "/differentiate/*" :route-name :differentiate
                       :interceptors [negotiated-type differentiate-intc]})
      (tp/table-route {:verb         :get :path "/api/differentiate/*" :route-name :differentiate-api
                       :interceptors [differentiate-api-intc]})
      }))

;---------------------------------------------------------------------------------------------------
; #todo => tupelo.pedestal ???

(def server-state (atom nil))

(defn service-fn []
  (grab ::http/service-fn @server-state))

(def default-service-map
  {::http/routes server-routes
   ::http/type   :jetty
   ::http/port   8890         ; default port
   ::http/host   "0.0.0.0"    ; *** CRITICAL ***  to make server listen on ALL IP addrs not just `localhost`
   ::http/join?  true         ; true => block the starting thread (want this for supervisord in prod); false => don't block
   })

(s/defn server-config!
  ([] (server-config! {}))
  ([server-opts :- tsk/KeyMap]
    (let [opts-to-use (glue default-service-map server-opts)]
      (reset! server-state (http/create-server opts-to-use)))))

(defn server-start! []
  (swap! server-state http/start))

(defn server-stop! []
  (swap! server-state http/stop))

(defn server-restart! []
  (server-stop!)
  (server-start!))
;---------------------------------------------------------------------------------------------------

(defn -main [& args]
  (println "main - enter")
  (if (empty? args)
    (server-config!)
    (let [port-str (first args)
          port     (tpar/parse-int port-str)]
      (server-config! {::http/port port})))
  (server-start!)
  (println "main - exit"))

