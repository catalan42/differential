(ns tst.hello.core
  (:use hello.core tupelo.core tupelo.test)
  (:require
    [clojure.edn :as edn]
    [clojure.string :as str]
    [io.pedestal.http :as http]
    [io.pedestal.http.route :as route]
    [io.pedestal.interceptor :as interceptor]
    [io.pedestal.interceptor.chain :as chain]
    [io.pedestal.test :as pedtst]
    [org.httpkit.client :as http-client]
    [schema.core :as s]
    [tupelo.pedestal :as tp]
    [tupelo.string :as ts]
    [tupelo.schema :as tsk]))

;---------------------------------------------------------------------------------------------------
; v1: Hello, World (http://pedestal.io/guides/hello-world)

(def tst-service-map
  {::http/port   8890
   ::http/host   "localhost" ; critical to make it listen on ALL IP addrs not just "localhost"
   ::http/join?  false ; true => block the starting thread (want this for supervisord in prod); false => don't block
   })

(defmacro with-server ; #todo => tupelo.pedestal ???
  "Start & stop the server, even if exception occurs."
  [& forms]
  `(try
     (server-config! tst-service-map)
     (server-start!) ; sends log output to stdout
     ~@forms
     (finally
       (server-stop!))))

(s/defn invoke-interceptors ; #todo => tupelo.pedestal
  "Given a context and a vector of interceptor-maps, invokes the interceptor chain
  and returns the resulting output context."
  [ctx :- tsk/KeyMap
   interceptors :- [tsk/KeyMap]] ; #todo => tupelo.pedestal & specialize to interceptor maps
  (let [pedestal-interceptors (mapv interceptor/map->Interceptor interceptors)]
    (chain/execute ctx pedestal-interceptors)))

(dotest
  (is= {:coeff 6 :exp 1} (differential {:coeff 3 :exp 2}))
  (is= {:coeff 12 :exp 2} (differential {:coeff 4 :exp 3}))
  (is= "3x^2" (term->str {:coeff 3 :exp 2} ))
  (is= "12x^3" (term->str {:coeff 12 :exp 3} ))
  (is= "4x^3+3x^2+2x+1" (terms->str [{:coeff 4 :exp 3}
                                    {:coeff 3 :exp 2}
                                    {:coeff 2 :exp 1}
                                    {:coeff 1 :exp 0}]))
  (is= "3x^2+1" (terms->str [{:coeff 0 :exp 3}
                            {:coeff 3 :exp 2}
                            {:coeff 0 :exp 1}
                            {:coeff 1 :exp 0}]))
  (is= "1" (terms->str [{:coeff 0 :exp 3}
                            {:coeff 0 :exp 2}
                            {:coeff 0 :exp 1}
                            {:coeff 1 :exp 0}]))
  (is= "0" (terms->str [{:coeff 0 :exp 3}
                       {:coeff 0 :exp 2}
                       {:coeff 0 :exp 1}
                       {:coeff 0 :exp 0}]))

  (server-config! tst-service-map)   ; mock testing w/o actually starting jetty
  (let [resp (pedtst/response-for (service-fn) :get "/api/differentiate/3/2/1") ]
    (is= {:differential "6x+2"} (json->edn (grab :body resp))) )
  (let [resp (pedtst/response-for (service-fn) :get "/api/differentiate/4/3/2/1") ]
    (is= {:differential "12x^2+6x+2"} (json->edn (grab :body resp))) )

  )


